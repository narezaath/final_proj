/* statistical compression */

/* time-critical part */

#include "tabrand.h"
#include "cdfstat.h"
#include "cfamily.h"
#include <assert.h>
#include <stdlib.h>

#include "encodecodeword.h"
#include "decodecodeword.h"


/* used by findbucket() */
static p_s_bucket * b_ptr_lo;	/* array of pointers to buckets - low part */
static p_s_bucket * b_ptr_hi;	/* array of pointers to buckets - high part */
static unsigned int
			b_lo_ptrs;		/* size of b_lo_ptrs */
static unsigned int
			b_hi_ptrs;

/* used by updatemodel() */
static unsigned int 
			wm_trigger;	/* threshold for halving counters, if trigger from taskparams <>0 */
						/* then wm_trigger=trigger, else wm_trigger is determined by set_wm_trigger()*/

/* used by w statdecompressrow() i statcompressrow() */
static unsigned int 
			waitcnt;		/* global counter of skipped model updates */
							
static unsigned int 
			_bpp;			/* local copy of bpp from taskparams */


/* return pointer to bucket containing context val */
static s_bucket * findbucket(const unsigned int val)
{
	assert(val<(0x1U<<_bpp));

	if (val<b_lo_ptrs)
		return b_ptr_lo[val];
	else
		return b_ptr_hi[(val-b_lo_ptrs)>>8];
}


/* update the bucket using just encoded curval */
static void updatemodel(s_bucket * const bucket, const unsigned int curval, 
						const unsigned int bpp)
{ 
	COUNTER * const pcounters=bucket->pcounters;
	unsigned int i;		
	unsigned int bestcode;
	unsigned int bestcodelen;

	/* update counters, find minimum */

	bestcode=bpp-1;
	bestcodelen=( pcounters[bestcode]+=GolombCodeLen(curval, bestcode) );

	for (i=bpp-2; i<bpp; i--) /* NOTE: expression i<bpp for signed int i would be: i>=0 !!! */
	{
		const unsigned int ithcodelen=( pcounters[i]+=GolombCodeLen(curval, i) );

		if (ithcodelen<bestcodelen)
		{
			bestcode=i;
			bestcodelen=ithcodelen;
		}
	}

	bucket->bestcode=bestcode; /* store the found minimum */

	if(bestcodelen>wm_trigger)	/* halving counters? */
		for (i=0; i<bpp; i++) 
			pcounters[i]>>=1;
}

/* for const waitmask perform compression */
/* args: context of the first symbol, array of symbols, length of the array, waitmask, */
/* output buffer and status vars for output buffer */
#ifndef ALT_STATCOMPRESSROWWM
	static void statcompressrowwm(const PIXEL firstcontext, const PIXEL uncompressedrow[],  
								const unsigned int width, const unsigned int waitmask,
								GRCode grcodes[], 
								unsigned int * const fullbytes, unsigned int * const bitsused)
	{
		unsigned int i;

		unsigned int stopidx; /* inner loop limit */
		assert(width);

		{	/* 0th */
				unsigned int codeword, codewordlen ;

				GolombCoding(uncompressedrow[0], 
							findbucket(firstcontext)->bestcode, 
							&grcodes[0].codeword, &grcodes[0].codewordlen);

				if (waitcnt)
					waitcnt--;
				else
				{
					waitcnt=(tabrand() & waitmask);
					updatemodel(findbucket(firstcontext), 
								uncompressedrow[0] , _bpp);
				}
		}

		i=1;
		stopidx=i+waitcnt;

		while (stopidx<width) /* encode sequence of symbols, update model after last symbol */
		{
			for (; i<=stopidx; i++)
			{
				unsigned int codeword, codewordlen ;

				GolombCoding(uncompressedrow[i], 
							findbucket(uncompressedrow[i-1])->bestcode, 
							&grcodes[i].codeword, &grcodes[i].codewordlen);
			}

			updatemodel(findbucket(uncompressedrow[stopidx-1]), 
							uncompressedrow[stopidx] , _bpp);
			stopidx=i+(tabrand() & waitmask);
		}

		for (; i<width; i++)	/* last sequence, not followed by model update */
		{
			unsigned int codeword, codewordlen ;

			GolombCoding(uncompressedrow[i], 
						findbucket(uncompressedrow[i-1])->bestcode, 
						&grcodes[i].codeword, &grcodes[i].codewordlen);
		}

		waitcnt=stopidx-width;
	}
#else
	static void statcompressrowwm(PIXEL context, const PIXEL uncompressedrow[],  
								const unsigned int width, const unsigned int waitmask,
								BYTE compressedrow[], 
								unsigned int * const fullbytes, unsigned int * const bitsused)
	{
		unsigned int i;

		unsigned int stopidx; /* inner loop limit */

		ENCODE_START(compressedrow, fullbytes, bitsused)

		
		assert(width);

		{	/* 0th */
				unsigned int codeword, codewordlen ;
				struct s_bucket * pbucket=findbucket(context);

				GolombCoding(context=uncompressedrow[0], 
							pbucket->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)

				if (waitcnt)
					waitcnt--;
				else
				{
					waitcnt=(tabrand() & waitmask);
					updatemodel(pbucket, context, _bpp);
				}
		}

		i=1;
		stopidx=i+waitcnt;

		while (stopidx<width) /* encode sequence of symbols, update model after last symbol */
		{
			struct s_bucket * pbucket;

			for (; i<=stopidx; i++)
			{
				unsigned int codeword, codewordlen ;
				pbucket=findbucket(context);

				GolombCoding(context=uncompressedrow[i], 
							pbucket->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)
			}

			updatemodel(pbucket, context, _bpp);
			stopidx=i+(tabrand() & waitmask);
		}

		for (; i<width; i++)	/* last sequence, not followed by model update */
		{
			unsigned int codeword, codewordlen ;
			struct s_bucket * pbucket=findbucket(context);

			GolombCoding(context=uncompressedrow[i], 
						pbucket->bestcode, 
						&codeword, &codewordlen);
			ENCODE(codeword, codewordlen)
		}

		ENCODE_STOP(compressedrow, fullbytes, bitsused)

		waitcnt=stopidx-width;
	}
#endif


static s_bucket * findbucket8bpp(const unsigned int val)
{
	assert(val<(0x1U<<_bpp));

	return b_ptr_lo[val];
}


#ifndef ALT_STATCOMPRESSROWWM
	static void statcompressrowwm8bpp(const BYTE firstcontext, const BYTE uncompressedrow[],  
								const unsigned int width, const unsigned int waitmask,
								BYTE compressedrow[], 
								unsigned int * const fullbytes, unsigned int * const bitsused)
	{
		unsigned int i;
		unsigned int stopidx;

		ENCODE_START(compressedrow, fullbytes, bitsused)

		assert(width);

		{
				unsigned int codeword, codewordlen ;

				GolombCoding(uncompressedrow[0], 
							findbucket8bpp(firstcontext)->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)

				if (waitcnt)
					waitcnt--;
				else
				{
					waitcnt=(tabrand() & waitmask);
					updatemodel(findbucket8bpp(firstcontext), 
									uncompressedrow[0] , _bpp);
				}
		}

		i=1;
		stopidx=i+waitcnt;

		while (stopidx<width)
		{
			for (; i<=stopidx; i++)
			{
				unsigned int codeword, codewordlen ;

				GolombCoding(uncompressedrow[i], 
							findbucket8bpp(uncompressedrow[i-1])->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)
			}

			updatemodel(findbucket8bpp(uncompressedrow[stopidx-1]), 
							uncompressedrow[stopidx] , _bpp);
			stopidx=i+(tabrand() & waitmask);
		}

		for (; i<width; i++)
		{
			unsigned int codeword, codewordlen ;

			GolombCoding(uncompressedrow[i], 
						findbucket8bpp(uncompressedrow[i-1])->bestcode, 
						&codeword, &codewordlen);
			ENCODE(codeword, codewordlen)
		}

		ENCODE_STOP(compressedrow, fullbytes, bitsused)

		waitcnt=stopidx-width;
	}
#else
	static void statcompressrowwm8bpp(BYTE context, const BYTE uncompressedrow[],  
								const unsigned int width, const unsigned int waitmask,
								BYTE compressedrow[], 
								unsigned int * const fullbytes, unsigned int * const bitsused)
	{
		unsigned int i;
		unsigned int stopidx;

		ENCODE_START(compressedrow, fullbytes, bitsused)

		assert(width);

		{
				unsigned int codeword, codewordlen ;
				struct s_bucket * pbucket=findbucket8bpp(context);

				GolombCoding(context=uncompressedrow[0], 
							pbucket->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)

				if (waitcnt)
					waitcnt--;
				else
				{
					waitcnt=(tabrand() & waitmask);
					updatemodel(pbucket, context, _bpp);
				}
		}

		i=1;
		stopidx=i+waitcnt;

		while (stopidx<width)
		{
			struct s_bucket * pbucket;

			for (; i<=stopidx; i++)
			{
				unsigned int codeword, codewordlen ;
				pbucket=findbucket8bpp(context);

				GolombCoding(context=uncompressedrow[i], 
							pbucket->bestcode, 
							&codeword, &codewordlen);
				ENCODE(codeword, codewordlen)
			}

			updatemodel(pbucket, context, _bpp);
			stopidx=i+(tabrand() & waitmask);
		}

		for (; i<width; i++)
		{
			unsigned int codeword, codewordlen ;
			struct s_bucket * pbucket=findbucket8bpp(context);

			GolombCoding(context=uncompressedrow[i], 
						pbucket->bestcode, 
						&codeword, &codewordlen);
			ENCODE(codeword, codewordlen)
		}

		ENCODE_STOP(compressedrow, fullbytes, bitsused)

		waitcnt=stopidx-width;
	}
#endif

/* ***************************************** decompress */

static int statdecompressrowwm(unsigned int context, PIXEL uncompressedrow[], 
						const unsigned int width, const unsigned int waitmask,
						struct bitinstatus *bs)
{
	unsigned int i;
	unsigned int stopidx;
	DECODE_START(bs)

	i=0;
	stopidx=i+waitcnt;

	while (stopidx<width)
	{
		struct s_bucket * pbucket;

		for (; i<=stopidx; i++)
		{
			unsigned int codewordlen;

			pbucket=findbucket(context);
			context=GolombDecoding(pbucket->bestcode, bits, &codewordlen);
			uncompressedrow[i]=context;
			DECODE_EATBITS(codewordlen)
		}
		
		updatemodel(pbucket, context, _bpp);

		stopidx=i+(tabrand() & waitmask);
	}

	for (; i<width; i++)
	{
		unsigned int codewordlen;
		struct s_bucket * pbucket;

		pbucket=findbucket(context);
		context=GolombDecoding(pbucket->bestcode, bits, &codewordlen);
		uncompressedrow[i]=context;
		DECODE_EATBITS(codewordlen)
	}

	waitcnt=stopidx-width;

	DECODE_STOP(bs)

	return 0;
}


static int statdecompressrowwm8bpp(unsigned int context, BYTE uncompressedrow[], 
						const unsigned int width, const unsigned int waitmask,
						struct bitinstatus *bs)
{
	unsigned int i;
	unsigned int stopidx;
	DECODE_START(bs)

	i=0;
	stopidx=i+waitcnt;

	while (stopidx<width)
	{
		struct s_bucket * pbucket;

		for (; i<=stopidx; i++)
		{
			unsigned int codewordlen;

			pbucket=findbucket8bpp(context);
			context=GolombDecoding(pbucket->bestcode, bits, &codewordlen);
			uncompressedrow[i]=context;
			DECODE_EATBITS(codewordlen)
		}
		
		updatemodel(pbucket, context, _bpp);

		stopidx=i+(tabrand() & waitmask);
	}

	for (; i<width; i++)
	{
		unsigned int codewordlen;
		struct s_bucket * pbucket;

		pbucket=findbucket8bpp(context);
		context=GolombDecoding(pbucket->bestcode, bits, &codewordlen);
		uncompressedrow[i]=context;
		DECODE_EATBITS(codewordlen)
	}

	waitcnt=stopidx-width;

	DECODE_STOP(bs)

	return 0;
}


/* not time-critical */


#include "cdftypes.h"
#include "exitit.h"
#include "bppmask.h"
#include "ceillog2.h"
#include "taskparams.h" 
#include "clalloc.h"


static COUNTER * pc=NULL;	/* helper for (de)allocation of arrays of counters */

static s_bucket * pb=NULL;	/* array of counters */


/* set by findmodelparams(), used by statfillstructures() */
static unsigned int 
			repfirst,		/* number of buckets of size equal to the 0th one */
			firstsize,		/* size of the 0th bucket */
			repnext,		/* how many consecutive buckets are of the same size */
			mulsize,		/* how many times to increase the size of the bucket */
			levels;			/* number of intensity levels */
static unsigned int
			nbuckets;		/* number of buckets */

static unsigned int
			ncounters;		/* number of counters allocated per bucket */

static unsigned int 
			wmidx,		/* current wait mask index */
			wmileft;	/* how many symbols to encode using current wmidx */

static const unsigned short besttrigtab[3][11]={ /* array of wm_trigger for waitmask and evol, used by set_wm_trigger() */
	/* 1 */	{ 550, 900, 800, 700, 500, 350, 300, 200, 180, 180, 160 },
	/* 3 */	{ 110, 550, 900, 800, 550, 400, 350, 250, 140, 160, 140 },
	/* 5 */	{ 100, 120, 550, 900, 700, 500, 400, 300, 220, 250, 160 }
	};



/* set wm_trigger knowing waitmask (param) and evol (glob)*/
void set_wm_trigger(unsigned int wm)
{
	if (trigger)
	{
		wm_trigger=trigger;
		return;
	}

	if (wm>10)
		wm=10;

	assert(evol<6);

	wm_trigger=besttrigtab[evol/2][wm];

	assert(wm_trigger<=2000);
	assert(wm_trigger>=1);
}


/* sets model params: ncodes, modelrows i finalbuckets*/
/* and model initialization params for statfillstructures(): repfirst/next, mulsize i evoluted */
void findmodelparams(const int bpp, const int evol)
{
	unsigned int 
		bsize,			/* bucket size */
		bstart, bend,	/* bucket start and end, range : 0 to levels-1*/
		repcntr;		/* helper */

	assert((bpp<=16) && (bpp>0));

	_bpp=bpp;

	if (bpp>8)
		ncounters=16;
	else
		ncounters=8;

	levels=0x1<<bpp;

	b_lo_ptrs=0;	/* ==0 means: not set yet */

	switch (evol)	/* set repfirst firstsize repnext mulsize */
	{
		case 1:						/* buckets contain following numbers of contexts:	1 1 1 2 2 4 4 8 8 ... */
			repfirst=3;	firstsize=1; repnext=2;	mulsize=2; break;	
		case 3:						/* 	1 2 4 8 16 32 64 ... */
			repfirst=1;	firstsize=1; repnext=1;	mulsize=2; break;	
		case 5:						/* 	1 4 16 64 256 1024 4096 16384 65536 */
			repfirst=1;	firstsize=1; repnext=1;	mulsize=4; break;	
		case 0:						/* obsolete */
		case 2:						/* obsolete */
		case 4:						/* obsolete */
			assert(0);
			exitit("findmodelparams(): evol value obsolete!!!", 100);
		default:
			assert(0);
			exitit("findmodelparams(): evol out of range!!!", 100);
	}

	nbuckets=0;
	repcntr=repfirst+1;	/* first bucket */
	bsize=firstsize;

	do	/* other buckets */
	{						
		if (nbuckets)			/* bucket start */
			bstart=bend+1;	
		else
			bstart=0;

		if (!--repcntr)			/* bucket size */
		{
			repcntr=repnext;
			bsize*=mulsize;
		}

		bend=bstart+bsize-1;	/* bucket end */
		if (bend+bsize>=levels)	/* if following bucked was bigger than current one */
			bend=levels-1;		/* concatenate them */

		if (!b_lo_ptrs)			/* array size not set yet? */
		{
			if (bend==levels-1)		/* this bucket is last - all in the first array */
				b_lo_ptrs=levels;
			else if (bsize>=256)	/* this bucket is allowed to reside in the 2nd table */
			{
				b_lo_ptrs=bstart;
				assert(bstart);		/* previous bucket exists */
			}
		}

		nbuckets++;
	}
	while (bend < levels-1);

	b_hi_ptrs=(255+levels-b_lo_ptrs)>>8; /* == ceil((levels-b_lo_ptrs)/256) */
}


/* fill the model */
/* counters are set to 0 by caloc()*/
void statfillstructures()
{
	unsigned int	
		bsize,	
		bstart,	
		bend,	
		repcntr,
		bnumber;

	COUNTER * freecounter=pc;	/* first free location in the array of counters */

	bnumber=0;

	repcntr=repfirst+1;	/* first bucket */
	bsize=firstsize;

	do	/* others */
	{						
		if (bnumber)	
			bstart=bend+1;	
		else
			bstart=0;

		if (!--repcntr)	
		{
			repcntr=repnext;
			bsize*=mulsize;
		}

		bend=bstart+bsize-1;	
		if (bend+bsize>=levels)	
			bend=levels-1;	

		pb[bnumber].bestcode=_bpp-1;
		pb[bnumber].pcounters=freecounter;
		freecounter+=ncounters;

		if (bstart<b_lo_ptrs)
		{
			unsigned int i;
			assert(bend<b_lo_ptrs);
			for(i=bstart; i<=bend; i++)
				b_ptr_lo[i]=pb+bnumber;
		}
		else	/* high part */
		{
			unsigned int i,iend;
			assert(bend>=b_lo_ptrs);

			i=(bstart-b_lo_ptrs)>>8;
			iend=(bend-b_lo_ptrs)>>8;
			for(;i<=iend; i++)
				b_ptr_hi[i]=pb+bnumber;
		}

		bnumber++;
	}
	while (bend < levels-1);

	assert(freecounter-pc==nbuckets*ncounters);

	return;
}


void statinitmodel(const int bpp, const int evol, const int init8bpp)
{
	assert(!pb);
	assert(!pc);
	assert(bpp>=1);
	assert(bpp<=16);

	findmodelparams(bpp, evol);

	if(init8bpp)
	{
		assert(!b_hi_ptrs);
		assert(b_lo_ptrs==(0x1U<<_bpp));
	}

	assert(b_lo_ptrs);

	b_ptr_lo=(p_s_bucket *)clamalloc(b_lo_ptrs * sizeof(p_s_bucket));
	if (!b_ptr_lo)
		exitit("no memory for model allocation", 4);
	if(b_hi_ptrs) /* it may be ==0 */
	{
		b_ptr_hi=(p_s_bucket *)clamalloc(b_hi_ptrs * sizeof(p_s_bucket));
		if (!b_ptr_hi)
			exitit("no memory for model allocation", 4);
	}
	pb=(s_bucket *)clamalloc( (size_t)nbuckets * sizeof(s_bucket) );
	if (!pb)
		exitit("no memory for model allocation", 4);
	/* memory for counters, not malloc, but calloc to set them to 0*/
	pc=(COUNTER *)clacalloc( (size_t)nbuckets, sizeof(COUNTER) * MAXNUMCODES );
	if (!pc)
		exitit("no memory for model allocation", 4);

	statfillstructures();
}


void statfreemodel()
{
	assert(pb);
	assert(pc);

	clafree(pc);
	pc=NULL;
	clafree(pb);
	pb=NULL;
	if(b_hi_ptrs)
		clafree(b_ptr_hi);
	clafree(b_ptr_lo);
}

/* exported functions */

void statinitcoder(const int bpp, const int maxclen,
				   const int evol, const int init8bpp)
{
	assert(bpp<=MAXNUMCODES);  
	statinitmodel(bpp, evol, init8bpp);
	familyinit(bpp, maxclen);
	stabrand();
	wmidx=wmistart;
	set_wm_trigger(wmidx);
	wmileft=wminext;
	waitcnt=0;
}


void statfreecoder()
{
	statfreemodel();
}


void statinitdecoder(const int bpp, const int maxclen, const int evol, const int width, struct bitinstatus *bs)
{
	assert(bpp<=MAXNUMCODES);
	statinitmodel(bpp, evol, 0);
	familyinit(bpp, maxclen);
	stabrand();
	wmidx=wmistart;
	set_wm_trigger(wmidx);
	wmileft=wminext;
	waitcnt=0;

	bitinstatusinit(bs);
}


void statfreedecoder()
{
	statfreemodel();	
}


void statcompressrow(PIXEL context, const PIXEL * uncompressedrow, unsigned int width,
					 GRCode *grcodes, unsigned int * fullbytes, unsigned int * bitsused)

{
	while ((wmimax > (int)wmidx) && (wmileft<=width)) /* wait mask index change inside the buffer */
	{
		if (wmileft)
		{
			statcompressrowwm(context, uncompressedrow, wmileft, bppmask[wmidx],
				              grcodes, fullbytes, bitsused);
			context=*(uncompressedrow+wmileft-1);
			uncompressedrow+=wmileft;
			width-=wmileft;
		}

		wmidx++;
		set_wm_trigger(wmidx);
		wmileft=wminext;
	}

	if (width)
	{
		statcompressrowwm(context, uncompressedrow, width, bppmask[wmidx],
			              grcodes, fullbytes, bitsused);
		if (wmimax > (int)wmidx)
			wmileft-=width;
	}

	assert((int)wmidx<=wmimax);
	assert(wmidx<=32);
	assert(wminext>0);
}

void statcompressrow8bpp(BYTE context, const BYTE * uncompressedrow, unsigned int width,
					     BYTE * compressedrow, unsigned int * fullbytes, unsigned int * bitsused)

{
	while ((wmimax > (int)wmidx) && (wmileft<=width))
	{
		if (wmileft)
		{
			statcompressrowwm8bpp(context, uncompressedrow, wmileft, bppmask[wmidx],
				                  compressedrow, fullbytes, bitsused);
			context=*(uncompressedrow+wmileft-1);
			uncompressedrow+=wmileft;
			width-=wmileft;
		}

		wmidx++;
		set_wm_trigger(wmidx);
		wmileft=wminext;
	}

	if (width)
	{
		statcompressrowwm8bpp(context, uncompressedrow, width, bppmask[wmidx],
			                  compressedrow, fullbytes, bitsused);
		if (wmimax > (int)wmidx)
			wmileft-=width;
	}

	assert((int)wmidx<=wmimax);
	assert(wmidx<=32);
	assert(wminext>0);
}

int statdecompressrow(PIXEL context, PIXEL * uncompressedrow, unsigned int width, struct bitinstatus *bs)
{
	int result;

	while ((wmimax > (int)wmidx) && (wmileft<=width))
	{
		if (wmileft)
		{
			result=statdecompressrowwm(context, uncompressedrow, wmileft, bppmask[wmidx], bs);
			context=*(uncompressedrow+wmileft-1);
			uncompressedrow+=wmileft;
			width-=wmileft;
		}

		wmidx++;
		set_wm_trigger(wmidx);
		wmileft=wminext;
	}

	if (width)
	{
		result=statdecompressrowwm(context, uncompressedrow, width, bppmask[wmidx], bs);
		if (wmimax > (int)wmidx)
			wmileft-=width;
	}

	assert((int)wmidx<=wmimax);
	assert(wmidx<=32);
	assert(wminext>0);

	return result;
}


int statdecompressrow8bpp(BYTE context, BYTE * uncompressedrow, unsigned int width, struct bitinstatus *bs)
{
	int result;

	while ((wmimax > (int)wmidx) && (wmileft<=width))
	{
		if (wmileft)
		{
			result=statdecompressrowwm8bpp(context, uncompressedrow, wmileft, bppmask[wmidx], bs);
			context=*(uncompressedrow+wmileft-1);
			uncompressedrow+=wmileft;
			width-=wmileft;
		}

		wmidx++;
		set_wm_trigger(wmidx);
		wmileft=wminext;
	}

	if (width)
	{
		result=statdecompressrowwm8bpp(context, uncompressedrow, width, bppmask[wmidx], bs);
		if (wmimax > (int)wmidx)
			wmileft-=width;
	}

	assert((int)wmidx<=wmimax);
	assert(wmidx<=32);
	assert(wminext>0);

	return result;
}

